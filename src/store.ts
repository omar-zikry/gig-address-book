import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import AddressBookSlice from "./containers/AddressBook/AddressBookSlice";

export const store = configureStore({
  reducer: {
    AddressBook: AddressBookSlice,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
