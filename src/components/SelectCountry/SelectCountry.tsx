import countriesList from "country-list";
import styled from "styled-components";

const SSelectInput = styled.select`
  height: 3rem;
  padding: 0 1rem;
  width: 100%;
  @media (max-width: 768px) {
    height: 2.5rem;
  }
`;

interface SelectCountryProps {
  handleChange: (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => void;
  value: string;
  onBlur: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void;
}

const SelectCountry = (props: SelectCountryProps) => {
  const countries = countriesList
    .getData()
    .sort((a, b) => ("" + a.name).localeCompare(b.name));

  const { handleChange, value, onBlur } = props;

  return (
    <div>
      <SSelectInput
        name="country"
        id="country"
        onChange={handleChange}
        value={value}
        onBlur={onBlur}
      >
        <option value="">Select a country</option>
        {countries.map((country) => (
          <option value={country.code} key={country.code}>
            {country.name}
          </option>
        ))}
      </SSelectInput>
    </div>
  );
};

export default SelectCountry;
