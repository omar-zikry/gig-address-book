import uniqueId from "lodash.uniqueid";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import SelectCountry from "../../SelectCountry/SelectCountry";
import useContactForm from "../../../utils/customHooks/useContactForm";
import { addContact } from "../../../containers/AddressBook/AddressBookSlice";
import Button from "../../Button/Button";
import { splitName } from "../../../utils/helperFunctions";

const SDisplayError = styled.p`
  color: ${(props) => props.theme.colors.red};
  font-size: 0.9rem;
  line-height: 1.5em;
`;

const SForm = styled.form`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 1rem;
  @media (max-width: 768px) {
    grid-template-columns: 1fr;
  }
`;

const STextInput = styled.input`
  width: 100%;
  height: 3rem;
  padding: 1rem;
  @media (max-width: 768px) {
    height: 1rem;
  }
`;

const SBtnGroup = styled.div`
  display: flex;
  justify-content: "space-between";
`;

interface AddContactFormProps {
  setIsOpen: (isOpen: boolean) => void;
}

const AddContactForm = (props: AddContactFormProps) => {
  const { setIsOpen } = props;

  const dispatch = useDispatch();

  const { inputs, handleChange, errors, handleBlur, touched } = useContactForm({
    name: "",
    email: "",
    country: "",
  });

  // check if all inputs are filled
  const isDisabled = !inputs.name || !inputs.email || !inputs.country;

  const handleSubmit = (e: React.SyntheticEvent): void => {
    e.preventDefault();

    const name = splitName(inputs.name);

    const id = uniqueId(
      inputs.email + inputs.country + name.lastName + name.firstName
    );

    // check there is no errors before dispatching the addContact action and closing the form
    if (!errors.name && !errors.email && !errors.country) {
      dispatch(
        addContact({
          id: id,
          ...name,
          email: inputs.email,
          country: inputs.country,
        })
      );
      setIsOpen(false);
    }
  };

  return (
    <SForm onSubmit={handleSubmit}>
      <div>
        <STextInput
          type="text"
          placeholder="Full Name"
          name="name"
          value={inputs.name}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        {touched.name && errors.name && (
          <SDisplayError>{errors.name}</SDisplayError>
        )}
      </div>
      <div>
        <STextInput
          type="email"
          placeholder="Email"
          name="email"
          value={inputs.email}
          onChange={handleChange}
          onBlur={handleBlur}
        />
        {touched.email && errors.email && (
          <SDisplayError>{errors.email}</SDisplayError>
        )}
      </div>
      <div>
        <SelectCountry
          value={inputs.country}
          handleChange={handleChange}
          onBlur={handleBlur}
        />
        {touched.country && errors.country && (
          <SDisplayError>{errors.country}</SDisplayError>
        )}
      </div>
      <SBtnGroup>
        <Button
          type="button"
          text="cancel"
          handleClick={() => setIsOpen(false)}
          preset="danger"
          style={{ marginRight: ".5rem" }}
        />
        <Button
          type="submit"
          text="Add Contact"
          preset="success"
          disabled={isDisabled}
        />
      </SBtnGroup>
    </SForm>
  );
};

export default AddContactForm;
