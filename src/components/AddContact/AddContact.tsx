import styled from "styled-components";
import { useState } from "react";
import addContactImg from "../../assets/imgs/add-user.svg";
import Button from "../Button/Button";
import AddContactForm from "./AddContactForm/AddContactForm";

export interface Inputs {
  name: string;
  email: string;
  country: string;
}

const SAddContact = styled.div`
  padding: 1rem 0;
  max-width: 1300px;
`;

const AddContact = () => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  return (
    <SAddContact>
      {!isOpen ? (
        <Button
          type="button"
          handleClick={() => setIsOpen(true)}
          icon={addContactImg}
          text="Add New Contact"
        />
      ) : (
        <AddContactForm setIsOpen={setIsOpen} />
      )}
    </SAddContact>
  );
};

export default AddContact;
