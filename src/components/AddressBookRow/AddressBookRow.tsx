import countriesList from "country-list";
import styled from "styled-components";
import { useState } from "react";
import { useDispatch } from "react-redux";
import SelectCountry from "../SelectCountry/SelectCountry";
import ActionBtn from "../ActionBtn/ActionBtn";
import {
  ContactItem,
  deleteContact,
  editContact,
} from "../../containers/AddressBook/AddressBookSlice";
import useContactForm from "../../utils/customHooks/useContactForm";
import { splitName } from "../../utils/helperFunctions";

interface SRowProps {
  isEditing: boolean;
}
// I am here passing isEditing prop cause in editing mode in mobile view the inputs can't fit horizontally
const SRow = styled.div<SRowProps>`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  padding: 1rem 0;
  border-bottom: 1px solid ${(props) => props.theme.colors.grey};
  gap: 0.5rem;
  @media (max-width: 768px) {
    font-size: 0.8rem;
    grid-template-columns: ${(props) =>
      props.isEditing ? "1fr" : "repeat(4, 1fr)"};
  }
`;

const SRowItem = styled.div`
  display: flex;
  align-items: center;
`;

const SBtnsGroup = styled.div`
  display: flex;
  button {
    margin-left: 1rem;
  }
`;

const SDisplayError = styled.p`
  color: ${(props) => props.theme.colors.red};
  font-size: 0.9rem;
  line-height: 1.5em;
`;

const SInput = styled.input`
  width: 100%;
  height: 3rem;
  padding: 1rem;
  @media (max-width: 768px) {
    height: 1rem;
  }
`;

const SName = styled.p`
  text-transform: capitalize;
`;
const SEmail = styled.p`
  text-transform: lowercase;
`;

interface AddressBookRowProps {
  contact: ContactItem;
}

const AddressBookRow = (props: AddressBookRowProps) => {
  const { contact } = props;

  const [isEditing, setIsEditing] = useState(false);

  const dispatch = useDispatch();

  // useContactForm is custom hook that can be used in my forms to handle change
  const { inputs, handleChange, errors, handleBlur, touched } = useContactForm({
    name: `${contact.firstName} ${contact.lastName}`,
    email: contact.email,
    country: contact.country,
  });

  const name = splitName(inputs.name);

  // here we just dispatch delete action
  const handleDelete = (id: string) => {
    dispatch(deleteContact(id));
  };

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    // check if there is no errors then make dispatch edit action and switch editing mode
    if (!errors.name && !errors.email && !errors.country) {
      dispatch(
        editContact({
          ...contact,
          ...name,
          email: inputs.email,
          country: inputs.country,
        })
      );

      setIsEditing(!isEditing);
    }
  };

  return (
    <form onSubmit={handleSubmit}>
      <SRow key={contact.id} isEditing={isEditing}>
        <SRowItem>
          {isEditing ? (
            <div>
              <SInput
                type="text"
                value={inputs.name}
                name="name"
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.name && errors.name && (
                <SDisplayError>{errors.name}</SDisplayError>
              )}
            </div>
          ) : (
            <SName>
              {contact.firstName} {contact.lastName}
            </SName>
          )}
        </SRowItem>
        <SRowItem>
          {isEditing ? (
            <div>
              <SInput
                type="email"
                value={inputs.email}
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.email && errors.email && (
                <SDisplayError>{errors.email}</SDisplayError>
              )}
            </div>
          ) : (
            <SEmail>{contact.email}</SEmail>
          )}
        </SRowItem>
        <SRowItem>
          {isEditing ? (
            <div>
              <SelectCountry
                value={inputs.country}
                handleChange={handleChange}
                onBlur={handleBlur}
              />
              {touched.country && errors.country && (
                <SDisplayError>{errors.country}</SDisplayError>
              )}
            </div>
          ) : (
            <p>{countriesList.getName(contact.country)}</p>
          )}
        </SRowItem>
        <SRowItem>
          <SBtnsGroup>
            {!isEditing ? (
              <>
                <ActionBtn
                  action="edit"
                  handleClick={() => setIsEditing(true)}
                />
                <ActionBtn
                  action="delete"
                  handleClick={() => handleDelete(contact.id)}
                />
              </>
            ) : (
              <ActionBtn
                action="cancel"
                handleClick={() => setIsEditing(false)}
              />
            )}
            <ActionBtn action="submit" className={!isEditing ? "none" : ""} />
          </SBtnsGroup>
        </SRowItem>
      </SRow>
    </form>
  );
};

export default AddressBookRow;
