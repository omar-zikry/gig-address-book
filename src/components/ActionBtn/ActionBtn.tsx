import styled from "styled-components";
import editIcon from "../../assets/imgs/pencil-alt-solid.svg";
import deleteIcon from "../../assets/imgs/trash-solid.svg";
import checkIcon from "../../assets/imgs/check-solid.svg";
import cancelIcon from "../../assets/imgs/cancel.svg";

const SButton = styled.button`
  width: 35px;
  height: 35px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${(props) => props.theme.colors.white};
  border: none;
  box-shadow: 0px 0px 15px 0px ${(props) => props.theme.colors.grey};
  cursor: pointer;
  transition: box-shadow 0.4s ease;
  &.none {
    display: none;
  }
  &:focus {
    outline: 0;
  }
  &:hover {
    box-shadow: 0px 0px 2px 0px ${(props) => props.theme.colors.grey};
  }
  img {
    height: 14px;
  }
`;

interface ActionBtnProps {
  action: "edit" | "delete" | "submit" | "cancel";
  handleClick?: () => void;
  className?: string;
}

const ActionBtn = (props: ActionBtnProps) => {
  const { action, handleClick, className } = props;

  return (
    <SButton
      type={action === "submit" ? "submit" : "button"}
      onClick={handleClick}
      className={className}
    >
      <img src={getIcon(action)} alt="" />
    </SButton>
  );
};

const getIcon = (action: string) => {
  let icon;
  switch (action) {
    case "edit":
      icon = editIcon;
      break;
    case "delete":
      icon = deleteIcon;
      break;
    case "submit":
      icon = checkIcon;
      break;
    case "cancel":
      icon = cancelIcon;
      break;
    default:
      icon = "";
  }
  return icon;
};

export default ActionBtn;
