import styled from "styled-components";

const SAddressBookHeader = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  border-bottom: 1px solid ${(props) => props.theme.colors.grey};
  padding-bottom: 1rem;
  @media (max-width: 768px) {
    font-size: 0.8rem;
  }
`;

const SAddressBookHeaderItem = styled.div`
  font-weight: 700;
`;

const AddressBookHeader = () => {
  return (
    <SAddressBookHeader>
      <SAddressBookHeaderItem>
        <p>Name</p>
      </SAddressBookHeaderItem>
      <SAddressBookHeaderItem>
        <p>Email</p>
      </SAddressBookHeaderItem>
      <SAddressBookHeaderItem>
        <p>Country</p>
      </SAddressBookHeaderItem>
    </SAddressBookHeader>
  );
};

export default AddressBookHeader;
