import styled from "styled-components";
import AddContact from "../AddContact/AddContact";

const STitle = styled.h2`
  font-size: 2rem;
  font-weight: 700;
  text-align: center;
  line-height: 1.4em;
  @media (max-width: 768px) {
    font-size: 1.2rem;
  }
`;

const SButtonContainer = styled.div`
  button {
    margin: auto;
  }
`;

const EmptyContactsScreen = () => {
  return (
    <>
      <STitle>How about we start adding some contacts? 😉</STitle>
      <SButtonContainer>
        <AddContact />
      </SButtonContainer>
    </>
  );
};

export default EmptyContactsScreen;
