import { useSelector } from "react-redux";
import { selectContact } from "../../containers/AddressBook/AddressBookSlice";
import AddressBookRow from "../AddressBookRow/AddressBookRow";

const AddressBookBody = () => {
  const contacts = useSelector(selectContact);

  return (
    <div>
      {contacts.map((contact) => (
        <AddressBookRow contact={contact} />
      ))}
    </div>
  );
};

export default AddressBookBody;
