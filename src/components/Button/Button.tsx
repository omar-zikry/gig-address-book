import styled from "styled-components";

interface ButtonProps {
  type: "submit" | "button" | "reset";
  handleClick?: () => void;
  text: string;
  preset?: "success" | "danger" | "material";
  style?: React.CSSProperties;
  icon?: string;
  disabled?: boolean;
}

const SButton = styled.button`
  background-color: ${(props) => props.theme.colors.white};
  color: ${(props) => props.theme.colors.black};
  border: none;
  border-radius: 0.3rem;
  padding: 1rem 2rem;
  width: fit-content;
  font-weight: 700;
  cursor: pointer;
  transition: all 0.4s ease;
  border-radius: 0.3rem;
  width: fit-content;
  border: 2px solid ${(props) => props.theme.colors.green};
  box-shadow: 0px 0px 15px 0px ${(props) => props.theme.colors.grey};
  display: flex;
  justify-content: center;
  align-items: center;
  max-height: 3.5em;
  img {
    width: 20px;
    margin-right: 0.4rem;
  }
  &.success {
    background-color: ${(props) => props.theme.colors.green};
    color: ${(props) => props.theme.colors.white};
    border: 2px solid ${(props) => props.theme.colors.green};
    &:hover {
      background-color: transparent;
      color: ${(props) => props.theme.colors.green};
    }
  }
  &.danger {
    background-color: ${(props) => props.theme.colors.white};
    color: ${(props) => props.theme.colors.red};
    border: 2px solid ${(props) => props.theme.colors.red};
    &:hover {
      background-color: ${(props) => props.theme.colors.red};
      color: ${(props) => props.theme.colors.white};
    }
  }
  &:disabled {
    opacity: 0.4;
  }
`;

const Button = (props: ButtonProps) => {
  const {
    text,
    type,
    handleClick,
    preset = "material",
    style,
    icon,
    disabled,
  } = props;
  return (
    <SButton
      type={type}
      onClick={handleClick}
      className={preset}
      style={style}
      disabled={disabled}
    >
      {icon && <img src={icon} alt="" />}
      {text}
    </SButton>
  );
};

export default Button;
