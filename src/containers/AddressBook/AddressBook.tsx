import { useSelector } from "react-redux";
import styled from "styled-components";
import AddressBookBody from "../../components/AddressBookBody/AddressBookBody";
import AddressBookHeader from "../../components/AddressBookHeader/AddressBookHeader";
import EmptyContactsScreen from "../../components/EmptyContactsScreen/EmptyContactsScreen";
import { selectContact } from "./AddressBookSlice";
import AddContact from "../../components/AddContact/AddContact";

const SAddressBook = styled.section`
  width: 90%;
  max-width: 1300px;
  margin: 0 auto;
  padding-top: 2rem;
`;

const AddressBook = () => {
  const contacts = useSelector(selectContact);
  return (
    <SAddressBook>
      {contacts?.length ? (
        <>
          <AddContact />
          <AddressBookHeader />
          <AddressBookBody />
        </>
      ) : (
        <EmptyContactsScreen />
      )}
    </SAddressBook>
  );
};

export default AddressBook;
