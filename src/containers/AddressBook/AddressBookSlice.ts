import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from "../../store";
import { getItem, saveItem } from "../../utils/storage";

export interface ContactItem {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  country: string;
}

interface AddressBookState {
  contacts: ContactItem[];
}

const initialState: AddressBookState = {
  // getItem is a function in utils that I made to get data from localStorage then parse it to object that can be used
  contacts: getItem("contacts") || [],
};

export const AddressBookSlice = createSlice({
  name: "counter",
  initialState,
  reducers: {
    addContact: (state, action: PayloadAction<ContactItem>) => {
      state.contacts = [...state.contacts, action.payload];

      // I am saving items to localStorage with every action dispatched
      saveItem("contacts", state.contacts);
    },
    deleteContact: (state, action: PayloadAction<string>) => {
      const filteredContacts = state.contacts.filter(
        (address) => address.id !== action.payload
      );

      state.contacts = filteredContacts;

      saveItem("contacts", state.contacts);
    },
    editContact: (state, action: PayloadAction<ContactItem>) => {
      const contactIndex = state.contacts.findIndex(
        (contact) => contact.id === action.payload.id
      );

      state.contacts[contactIndex] = action.payload;

      saveItem("contacts", state.contacts);
    },
  },
});

export const {
  addContact,
  deleteContact,
  editContact,
} = AddressBookSlice.actions;

export const selectContact = (state: RootState) => state.AddressBook.contacts;

export default AddressBookSlice.reducer;
