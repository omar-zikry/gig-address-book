import styled from "styled-components";
import AddressBook from "../AddressBook/AddressBook";

const STitle = styled.h1`
  text-align: center;
  font-weight: 700;
  font-size: 2rem;
  line-height: 1.5em;
  padding-top: 5rem;
`;
function App() {
  return (
    <>
      <STitle>Address Book</STitle>
      <AddressBook />
    </>
  );
}

export default App;
